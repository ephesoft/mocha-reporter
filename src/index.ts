import * as Mocha from 'mocha';
import { JUnit } from './JUnit';
import { SonarCloud } from './SonarCloud';

/**
 * Ephesoft reporter that outputs normal console details along with report files Bitbucket and SonarCloud.
 */
class EphesoftReporter extends Mocha.reporters.Base {
    constructor(runner: Mocha.Runner, options: Mocha.MochaOptions) {
        super(runner, options);

        // JUnit for Bitbucket
        new JUnit(runner, options);

        // SonarCloud Generic Test Execution Reporter
        new SonarCloud(runner, options);

        // Default Console Output
        new Mocha.reporters.Spec(runner, options);
    }
}

// https://www.typescriptlang.org/docs/handbook/modules.html#export-all-as-x
export = EphesoftReporter;
