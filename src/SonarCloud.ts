import * as Mocha from 'mocha';
import path = require('path');
import { createElement, createOpenElement, escape } from './xmlUtils';
import { mkdirSync, createWriteStream, WriteStream } from 'fs';

// Using constants from Mocha to ensure forward compatiblity (per docs...)
const { EVENT_RUN_END, EVENT_TEST_FAIL, EVENT_TEST_PASS, EVENT_TEST_PENDING } = Mocha.Runner.constants;

// Not part of the default set
const STATE_FAILED = 'failed';

/**
 * Constructs a new `SonarCloud` reporter instance.
 * SonarCloud spec: https://docs.sonarqube.org/latest/analysis/generic-test/
 * Mocha custom reporter: https://mochajs.org/api/tutorial-custom-reporter.html
 *
 * @public
 * @class
 * @extends MOCHA.reporters.Base
 * @param {Runner} runner - Instance triggers reporter actions.
 * @param {Object} [options] - runner options
 */
export class SonarCloud extends Mocha.reporters.Base {
    public description: string = 'SonarCloud Generic Test Execution Reporter';
    private fileStream: WriteStream;
    private output: string = './.sonarcloud/default.xml';
    private testsByFile: Record<string, Mocha.Test[]> = {};

    constructor(runner: Mocha.Runner, options: Mocha.MochaOptions) {
        super(runner, options);

        // Create directory and fileStream
        this.setFileName(options);
        mkdirSync(path.dirname(this.output), {
            recursive: true
        });
        this.fileStream = createWriteStream(this.output);

        // Wire up Listeners
        runner.on(EVENT_TEST_PENDING, (test) => {
            this.addTest(test);
        });

        runner.on(EVENT_TEST_PASS, (test) => {
            this.addTest(test);
        });

        runner.on(EVENT_TEST_FAIL, (test) => {
            this.addTest(test);
        });

        runner.once(EVENT_RUN_END, () => {
            this.write(
                createOpenElement('testExecutions', {
                    version: '1'
                })
            );

            for (const file in this.testsByFile) {
                // Add File
                this.write(createOpenElement('file', { path: file }));

                // Add Tests
                this.testsByFile[file].forEach((t) => {
                    this.test(t);
                });

                // Close File
                this.write('</file>');
            }
            this.write('</testExecutions>');
            this.fileStream.end();
        });
    }

    setFileName(options: Mocha.MochaOptions) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-assignment
        const spec = (options as any).spec[0];

        // See if the spec matches an Ephesoft format
        if (typeof spec === 'string') {
            const regex = /^test\/(?<type>(unit|integration|dao))\/\*\*\/\*\.test\.ts$/s;
            const matches = regex.exec(spec);

            // Name output file based on unit, integration, or dao tests...
            if (matches?.groups?.['type']) {
                this.output = `./.sonarcloud/${matches?.groups?.['type']}.xml`;
            }
        }
    }

    write(line: string) {
        this.fileStream.write(line + '\n');
    }

    addTest(test: Mocha.Test) {
        const file = path.relative('', test.file ?? '');
        if (file) {
            // Check to see if this file already has tests
            if (this.testsByFile[file]) {
                this.testsByFile[file].push(test);
            } else {
                this.testsByFile[file] = [test];
            }
        }
    }

    /**
     * Create XML element for each test
     *
     * @param {Test} test
     */
    test(test: Mocha.Test) {
        const attrs = {
            name: test.title,
            duration: test.duration ?? 0 // Value in milliseconds
        };

        if (test.state === STATE_FAILED || test.err) {
            const err = test.err;

            // Create Element
            this.write(
                createElement('testCase', attrs, createElement('failure', { message: escape(err?.message) }, escape(err?.stack)))
            );
        } else if (test.isPending()) {
            this.write(createElement('testCase', attrs, createElement('skipped', { message: 'Test was skipped' })));
        } else {
            this.write(createElement('testCase', attrs));
        }
    }
}
