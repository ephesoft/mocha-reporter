import htmlEntities = require('he');

export function escape(text: string | undefined): string {
    if (text === undefined || text === null) {
        return '';
    }
    return htmlEntities.encode(String(text), { useNamedReferences: false });
}

/**
 * Create Open XML Element
 *
 * @param name
 * @param attrs XML attributes as a record
 * @return {string}
 */
export function createOpenElement(name: string, attrs: Record<string, string | number>): string {
    return `<${name}${getXMLAttributeString(attrs)}>`;
}

/**
 * Create XML Element
 *
 * @param name
 * @param attrs XML attributes as a record
 * @param content
 * @return {string}
 */
export function createElement(name: string, attrs: Record<string, string | number>, content?: string): string {
    if (content) {
        return `<${name}${getXMLAttributeString(attrs)}>` + content + `</${name}>`;
    }
    return `<${name}${getXMLAttributeString(attrs)}/>`;
}

function getXMLAttributeString(attrs: Record<string, string | number>): string {
    const pairs = [];

    // Add XML attributes
    for (const key in attrs) {
        if (Object.prototype.hasOwnProperty.call(attrs, key)) {
            pairs.push(`${key}="${escape(attrs[key].toString())}"`);
        }
    }

    // Include space before first attribute
    return pairs.length ? ' ' + pairs.join(' ') : '';
}
