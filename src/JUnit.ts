import * as Mocha from 'mocha';
import path = require('path');
import { createElement, createOpenElement, escape } from './xmlUtils';
import { mkdirSync, createWriteStream, WriteStream } from 'fs';

// Using constants from Mocha to ensure forward compatiblity (per docs...)
const { EVENT_RUN_END, EVENT_TEST_FAIL, EVENT_TEST_PASS, EVENT_TEST_PENDING } = Mocha.Runner.constants;

// Not part of the default set
const STATE_FAILED = 'failed';

/**
 * Constructs a new `JUnit` reporter instance.
 * JUnit spec: https://www.ibm.com/docs/en/developer-for-zos/9.1.1?topic=formats-junit-xml-format
 * Mocha custom reporter: https://mochajs.org/api/tutorial-custom-reporter.html
 * Bitbucket Support: https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/
 *
 * @public
 * @class
 * @extends MOCHA.reporters.Base
 * @param {Runner} runner - Instance triggers reporter actions.
 * @param {Object} [options] - runner options
 */
export class JUnit extends Mocha.reporters.Base {
    public description: string = 'JUnit XML Reporter';
    private fileStream: WriteStream;
    private output: string;
    private suiteName: string; // the name of the test suite, as it will appear in the resulting XML file

    constructor(runner: Mocha.Runner, options: Mocha.MochaOptions) {
        super(runner, options);
        const tests: Mocha.Test[] = [];

        // the default name of the test suite if none is provided
        const DEFAULT_SUITE_NAME = 'Mocha Tests';

        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment
        this.output = options?.reporterOptions?.output ?? './test-reports/junit.xml';

        mkdirSync(path.dirname(this.output), {
            recursive: true
        });
        this.fileStream = createWriteStream(this.output);

        // get the suite name from the reporter options (if provided) or use default
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment
        this.suiteName = options?.reporterOptions?.suiteName ?? DEFAULT_SUITE_NAME;

        runner.on(EVENT_TEST_PENDING, function (test) {
            tests.push(test);
        });

        runner.on(EVENT_TEST_PASS, function (test) {
            tests.push(test);
        });

        runner.on(EVENT_TEST_FAIL, function (test) {
            tests.push(test);
        });

        runner.once(EVENT_RUN_END, () => {
            this.write(
                createOpenElement('testsuite', {
                    name: this.suiteName,
                    tests: runner.stats?.tests ?? 0,
                    failures: runner.stats?.failures ?? 0,
                    skipped: (runner.stats?.tests ?? 0) - (runner.stats?.failures ?? 0) - (runner.stats?.passes ?? 0), // Not in offical Spec
                    timestamp: new Date().toUTCString(), // Not in offical Spec
                    time: (runner.stats?.duration ?? 0) / 1000 || 0
                })
            );

            tests.forEach((t) => {
                this.test(t);
            });

            this.write('</testsuite>');
            this.fileStream.end();
        });
    }

    write(line: string) {
        this.fileStream.write(line + '\n');
    }

    /**
     * Create XML element for each test
     *
     * @param {Test} test
     */
    test(test: Mocha.Test) {
        const attrs = {
            parent: test.parent?.title ?? '', // Not in offical Spec, but usefull data as we don't currently build up separate suites
            name: test.title,
            time: (test.duration ?? 0) / 1000 || 0
        };

        if (test.state === STATE_FAILED || test.err) {
            const err = test.err;

            // Create Element
            this.write(createElement('testcase', attrs, createElement('failure', {}, escape(`${err?.message}\n${err?.stack}`))));
        } else if (test.isPending()) {
            this.write(createElement('testcase', attrs, createElement('skipped', {})));
        } else {
            this.write(createElement('testcase', attrs));
        }
    }
}
