# Changelog

## 2.8.0 (2022-10-21)
- Removed internal package references

## 2.7.0 (2022-10-05)
- Updated to template version 6.0.2
- Updated npm references

## 2.6.0 (2022-09-26)
- Updated to template version 5.2.0
- Updated npm references
- Fixed npm audit warning in bitbucket-pipelines.yml
- Updated to publish to NPM instead of MyGet

## 2.5.1 (2022-04-13)
- Fixed skipped SonarCloud xml output

## 2.5.0 (2022-04-13)
- Added dao as a known test type

## 2.4.0 (2022-03-22)
- Added SonarCloud xml reporter

## 2.3.0 (2022-03-09)
- Updated to use common SDK format and re-wrote in TypeScript
- Updated reporterOptions.output to have a default of `./test-reports/junit.xml`
- Updated output to remove unused attributes not in JUnit specification
- Updated output to include parent on each testcase

## 2.2.0 (2021-11-24)
- Updated npm references
- Updated based on sdk template 2.5.0 (these are very different types of projects)

## 2.1.1 (2021-04-27)
- Fixed build script (publish step was missing npm ci call)

## 2.1.0 (2021-04-27)
- Updated npm references
- Updated to use integration.test.sdk

## 2.0.2 (2020-10-27)
- Fixed bug where test errors (i.e. prehook) were not being reported in the xunit report. Because the test object has an err property, but no state property, the report was falling through to a pending test case. This fix adds a check for the err property and reports correctly now.

## 2.0.1 (2020-10-02)
- Added our own xunit custom reporter based on Mocha's XUnit class. There were a couple issues with their reporter that forced us to do this.  Specifically, failure attribute on testsuite xml element was not being used, but rather errors was (see https://github.com/mochajs/mocha/issues/4483).

## 2.0.0 (2020-10-02)
- BREAKING: Migrated from mocha-junit-reporter to built-in xunit reporter

## 1.2.0 (2020-09-04)
- Updated npm references

## 1.1.0 (2020-08-28)
- Added package tests
- Updated build to support beta releases and to test pull requests
- Added husky to run tests before push
- Updated npm references
- Added audit and test scripts to package.json

## 1.0.0 (2020-04-03)
- Added files for initial release

- - - - -
* Following [Semantic Versioning](https://semver.org/)
* [Changelog Formatting](https://ephesoft.atlassian.net/wiki/spaces/ZEUS1/pages/1189347481/Changelog+Formatting)
* [Major Version Migration Notes](MIGRATION.md)