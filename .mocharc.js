module.exports = {
    timeout: 30000,
    require: ['ts-node/register'],
    reporter: './dist/index.js' // Use the built reporter in dist
};