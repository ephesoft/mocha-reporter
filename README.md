# @ephesoft/mocha-reporter

Custom Mocha reporter to combine JUnit (Bitbucket), SonarCloud and spec (console output) reporters.  Allowing report files to be generated for consumption by our tooling while regular terminal output is preserved.

## Installation

```sh
npm install --save-dev mocha @ephesoft/mocha-reporter
```

## Configuration

Add the following to `.mocharc.js`

```js
module.exports = {
  ...
  reporter: '@ephesoft/mocha-reporter'
}
```

## References
- [Mocha custom reporter](https://mochajs.org/api/tutorial-custom-reporter.html)
- [JUnit spec](https://www.ibm.com/docs/en/developer-for-zos/9.1.1?topic=formats-junit-xml-format)
- [Bitbucket Support](https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/)
- [SonarCloud spec](https://docs.sonarqube.org/latest/analysis/generic-test/)

## Potential Future Enhancements
1. The JUnit reporter could be updated to use testsuites grouping up the tests by file like the SonarCloud reporter
2. Both reporters could be updated to user a proper XML utility for generating the elements